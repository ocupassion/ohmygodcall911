<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->string('heading', 100);
            $table->string('description', 200);
            $table->string('badge', 100);
            $table->string('banner', 300);
            $table->string('color', 7);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        //Pivot Table to allow courses linked to institutions

        Schema::create('course_institution', function (Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->integer('institution_id')->unsigned();
            $table->timestamps();
        });
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
        Schema::drop('course_institution');

    }
}
