<?php

use App\Institution;
use App\Course;
use App\User;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        $this->call("InstitutionSeeder");
        $this->call("UserSeeder");
        $this->call("CourseSeeder");
    }
}

Class CourseSeeder extends Seeder
{
    public function run()
    {
        // ** Business course **

        DB::table('courses')->truncate();
        DB::table('course_institution')->truncate();
        DB::table('modules')->truncate();

        $user = User::first();

        $course1 = Course::create([
            'title' => "Starting Your Own Business",
            'description' => "Here you will find what starting your own business means, how to take an idea from the start, identify customers, find the ultimate users and create a product that you can sell.",
            'heading' => "Turn an idea into a business",
            'badge' => "/assets/img/courses/Startingyourownbusiness/badge.png",
            'banner' => "/assets/img/courses/Startingyourownbusiness/banner.png",
            'color' => '#60bddb',
            'user_id' => $user->id
        ]);

        $course2 = Course::create([
            'title' => "Customer Service",
            'description' => "Here we have the Celtic Manor Resort Customer Service guide",
            'heading' => "Celtic Manor resort standard",
            'badge' => "/assets/img/courses/CMRCustomerService&HospitalityCourse/badge.png",
            'banner' => "/assets/img/courses/CMRCustomerService&HospitalityCourse/banner.png",
            'color' => '#e6b0aa',
            'user_id' => $user->id
        ]);

        $course3 = Course::create([
            'title' => "Customer Service",
            'description' => "ITEC module that covers unit 101",
            'heading' => "Creating a good impression",
            'badge' => "/assets/img/courses/ITECCustomerService/badge.png",
            'banner' => "/assets/img/courses/ITECCustomerService/banner.png",
            'color' => '#81c8bd',
            'user_id' => $user->id
        ]);


        // link to an institution
        $id = Institution::first()->id;
        $course1->institutions()->attach($id);
        $course2->institutions()->attach($id);


        $id = Institution::whereSubdomain('Celticuk')->first()->id;
        $course3->institutions()->attach($id);


        // now create some modules
        $course1->modules()->create([
            'title' => "What do you know?",
            'description' => "Pre question to validate their knowledge",
            'order' => 1,
            'resource' => null
        ]);


        $course1->modules()->create([
            'title' => "What is an Entrepreneur",
            'description' => "Learn what it takes to be an entrepreneur.",
            'order' => 2,
            'resource' => null
        ]);
    }
}

/**
* 	Create an example Institution 
*/
class InstitutionSeeder extends Seeder
{
	
	public function run()
	{
        DB::table('institutions')->truncate();

		Institution::create([
			'name' => "The Alacrity Foundation",
			'subdomain' => "alacrityuk",
			'logo_path' => "/public/assets/img/institution/alacrityuk/logo.png",
			'banner_path' => "/assets/img/institution/alacrityuk/banner.png",
		]);

		Institution::create([
			'name' => "Celtic Manor",
			'subdomain' => "Celticuk",
			'logo_path' => "/public/assets/img/institution/Celticuk/logo.png",
			'banner_path' => "/assets/img/institution/Celticuk/banner.png",
		]);
	}
}

/**
* 	Create an example User 
*/
class UserSeeder extends Seeder
{
	public function run()
	{
        DB::table('users')->truncate();
        DB::table('institution_user')->truncate();

		// Create a user  
        $newUser = User::create([
            'name' => 'Ben',
            'email' => 'ben@occupassion.co.uk',
            'password' => Hash::make('ben1234'),
            'lastname' => 'Willson',
            'profile_url' => 'https://www.shoptab.net/blog/wp-content/uploads/2014/07/profile-circle.png'
        ]);

        $newUser->institutions()->attach(Institution::first()->id);
	}
}

