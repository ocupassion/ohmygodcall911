'use strict';
var app = angular.module("occupassion");

app.controller("CoursesCtrl",CoursesCtrl);

// Inject dependencies
CoursesCtrl.$inject = ['$scope', '$http', '$state', 'courseService'];

function CoursesCtrl($scope, $http, $state, courseService){

    var courseController = this;

	// this function communicates with the remote server, to
	// pull down all courses that are relevant for this user,
	// and the institution that they belong to
    courseService.getCourses()
        .then(function (courses) {
            courseController.courses = courses;
        });

    courseController.selectCourse = function(course) {
    	courseService.courseData = course;
    	$state.go('course', {'courseId': course.id});
    };
}