angular
	.module('occupassion')
	.factory('institutionService',institutionService);

	institutionService.$inject = ['$http'];

	function institutionService($http){
	 	return{
	 		getLogo:getLogo,
	 		getBanner:getBanner

	 	}

	 	function getLogo(){
	 		return $http.get('api/get/portal/logo')
	 		.then(function success(response){
	 			return response
	 		},function error(){
	 			console.log('Error')
	 		});
	 	}

	 	function getBanner(){
	 		return $http.get('api/get/portal/banner')
	 		.then(function success(response){
	 			return response
	 		},function error(){
	 			console.log('Error')
	 		});
	 	}
	 }

