'use strict'
var app = angular.module("occupassion");

app.controller('FrameController', function($scope,$rootScope,authService,$state) 
{

	var frame = this;

	frame.logout = function() {
		authService.logoutUser();
		$state.go('login');
	}
})