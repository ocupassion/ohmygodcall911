angular.module("occupassion",['satellizer','ui.router'])
.config(function($stateProvider,$authProvider,$urlRouterProvider){

	// The login url
	$authProvider.loginUrl = '/api/login';

	// Default state 
  $urlRouterProvider.otherwise('/login');

	// URL based nav system
	$stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'assets/views/auth/login.html',
        controller: 'LogInCtrl',
        controllerAs: 'loginScope',
        authenticate: false
      })
      .state('courses', {
        url: '/courses',
        templateUrl: 'assets/views/main/courses.html',
        controller: 'CoursesCtrl',
        controllerAs: 'coursesScope',
        authenticate: true
      })
      .state('course',{
        url:'/course/{courseId}/',
        templateUrl: 'assets/views/main/course.html',
        controller: 'CourseCtrl',
        controllerAs: 'courseScope',
        authenticate: true
      })
      .state('content',{
        url:'/course/{courseId}/module/{moduleId}',
        templateUrl: 'assets/views/main/content.html',
        controller: 'ContentCtrl',
        controllerAs: 'contentScope',
        authenticate: true
      })
})
.run(function($rootScope, $state, $stateParams, authService) {
	
	$rootScope.$state = $state;

  $rootScope.$on('$stateChangeStart', function(event, toState) {

      // Try and get a user again from token if i don't have one in scope
      if(!$rootScope.user && toState.authenticate){
        if(localStorage.getItem('satellizer_token') != null){
  		    gotTokenNoUser();
        }else{
          redirectLogin();
        }
      }

      function gotTokenNoUser(){ 
        // Go get me my user details
        authService.getUser()
        .then(function(response){
          if(!$rootScope.user){
            redirectLogin();
          }
        });
      }

      function redirectLogin(){
        // Preventing the default behavior allows us to use $state.go
        // to change states
        event.preventDefault();

        $state.go('login');
      }
  });
});

