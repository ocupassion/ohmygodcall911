'use strict'
angular
  .module('occupassion')
  .factory('authService',authService)

  authService.$inject=['$http', '$auth', '$rootScope'];

  function authService($http, $auth, $rootScope){
    return{
      loginUser:loginUser,
      getUser:getUser,
      logoutUser:logoutUser
    }

    // Public functions
    
    function loginUser(email, password){
     
      // Users details 
      var credentials = {
        email: email,
        password: password
      }

      // Use the $auth.login to get user token 
      return $auth.login(credentials)
      .then(getUser, errorLog)
    }

    function getUser(response){
      return $http.get('api/get/user')
      .then(extractUser, errorLog);
    }

    function extractUser(response){
      // Set the current user
      $rootScope.user = response.data;
      return response;
    }

    function logoutUser(){
      // logout the user using the $auth.logout 
      // process (removes token)
      return $auth.logout()
      .then(removeUser)
    }

    // Private functions

    function removeUser(){
      // Remove user from rootScope
      $rootScope.user = null;
      return
    }

    function errorLog(error){
      // Return error response
      return error;
    }
  };
  


