angular
	.module('occupassion')
	.service('alertService',alertService);

	alertService.$inject = [];
	function alertService(){
		var alertService = this;

		alertService.alertMessage = null;
		alertService.alertClass = null;
		alertService.showAlert = false;
	};