'use strict';
var app = angular.module("occupassion");

app.controller("CourseCtrl",CourseCtrl);

// Inject dependencies
CourseCtrl.$inject = ['$scope', '$rootScope', 'courseService', '$state'];

function CourseCtrl($scope, $rootScope, courseService, $state){

    var courseController = this;

	if(!courseService.courseData){
	    courseService.getCourse().then(function(response){
	    	courseController.course = courseService.courseData;
	    });
	}else{
		courseController.course = courseService.courseData;
	}

	courseService.getModules()
	.then(function(response){
		courseController.modules = courseService.courseModules;
	});

	courseController.enrolCourse = function(){
		courseService.enrolCourse();
	}

	courseController.startModule = function(courseId, moduleId){
		courseService.startModule(moduleId)
		.then(function(response){
			if(!response.data.error){
				$state.go('content', {'courseId':courseId, 'moduleId':moduleId});
			}
		})
	}
}
