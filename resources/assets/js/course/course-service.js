'use strict'
angular
  .module('occupassion')
  .factory('courseService', courseService)

  courseService.$inject=['$http', '$rootScope', '$stateParams'];

  function courseService($http, $rootScope, $stateParams){
    return{
      courseData: null,
      courseModules: null,
      getCourse:getCourse,
      getCourses:getCourses,
      getModules:getModules,
      enrolCourse:enrolCourse,
      startModule:startModule,
    }

    // Public functions
    function getCourses(){
      self = this;

      return $http.get('/api/courses')
        .then(function (response) {
          return response.data;
        }, function(response){
          console.log(response.data.error);
        });
    }

    function getCourse(){
      self = this;

      // If course is not set attempt to get course
      return callGetCourse($stateParams.courseId)
      .then(function(response){
        self.courseData = response;
        return response;
      });
    }

    function getModules(){
      self = this;

      self.courseModules = null;

      return $http.get('api/course/' + $stateParams.courseId + '/modules')
      .then(function(response) {
        self.courseModules = response.data;
        return response.data;
      }, function(response){
        console.log(response.data.error);

        return response;
      })
    }

    function enrolCourse(){
      var self = this;
      return $http.post('api/course/' + $stateParams.courseId + '/enrol', {})
        .then(function (response) {
          self.courseData.user_in_progress = 1;
          return response;
        }, function(response){
          console.log(response.data.error);
        });
    }

    function startModule(moduleId){
      var self = this;
      return $http.post('api/course/' + $stateParams.courseId + '/module/' + moduleId +'/start', {})
        .then(function (response) {
          return response;
        }, function(response){
          console.log(response.data.error);
          return response;
        });

    }

    // Private functions
    function callGetCourse(courseId){
      // Get course
      return $http.get('/api/course/' + courseId)
        .then(function (response) {
          return response.data;
        }, function(response){
          console.log(response.data.error);

          return response;
        });
    } 
  };
  


