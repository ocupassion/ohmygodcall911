'use strict'
var app = angular.module("occupassion");

app.controller("LogInCtrl",LogInCtrl);

// Inject dependencies
LogInCtrl.$inject = ['$scope', '$http', 'authService', '$state','institutionService','alertService'];

function LogInCtrl($scope, $http, authService, $state, institutionService,alertService){
	var logIn = this;
	logIn.email = '';
	logIn.password = '';
	institutionService.getBanner().then(function(response){
		logIn.banner = response.data;

	});

	// Login functions
	logIn.login = function() {
		if(logIn.email != '' && logIn.password != ''){
			authService.loginUser(logIn.email, logIn.password)
			.then(function(response) {
				if(response.data.error){
					alertService.showAlert = true;
					alertService.alertMessage = response.data.error;
					alertService.alertClass = 'alert-message';
				}else{
					logIn.errorMessages = null;
					$state.go('courses');
				}
			});
		}
	}
}