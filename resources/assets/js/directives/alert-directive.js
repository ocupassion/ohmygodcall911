angular
	.module('occupassion')
	.directive('alertMessage',alertMessage);
  function alertMessage(){
    var directive = {
			restric:'E',
			template:'<div class="{{alert.alertService.alertClass}}" ng-if="alert.alertService.showAlert"><p>{{alert.alertService.alertMessage}}</p></div>',
			controller:"AlertCtrl as alert",
			replace:true,
			bindToController:true
    };
    return directive;
  };
