<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Occupassion</title>
  <link rel="stylesheet" href="assets/css/app.min.css">
  <link rel="stylesheet" href="assets/dependencies/lib/node_modules/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css"> 
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,600' rel='stylesheet' type='text/css'>

</head>
<body ng-app="occupassion" ng-controller="FrameController as frame">
  <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="main-container">
        <div class="navbar-header">
          <a class="navbar-brand navbar-link" ><b>OCC</b>UPASSION</a>
        </div>
        <div>
          <ul class="nav navbar-nav" ng-if="user.name != null">
            <li class="dropdown" >
              <a  ui-sref="courses" role="button" class="navbar-link navbar-link-hover hvr-underline-from-center">Courses</a>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <ul class="nav navbar-nav" ng-if="user.name != null">
              <li class="dropdown">
                <a class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img ng-src="{{ user.profile_url }}">{{ user.name }}{{ user.lastname }}<span class="caret"></span></a>
                <ul class="dropdown-menu pointer">
                  <li><a><i class="fa fa-user pointer" aria-hidden="true"></i> Profile</a></li>
                  <li><a><i class="fa fa-wrench pointer" aria-hidden="true"></i> Account Settings</a></li>
                  <li role="separator" class="divider"></li>
                  <li ng-click="frame.logout()"><a ><i class="fa fa-sign-out pointer" aria-hidden="true" ></i> LogOut</a></li>
                </ul>
              </li>
            </ul>
            <ul class="nav navbar-nav" ng-if="user.name == null">
              <li class="dropdown">
                <a class="navbar-link"><i class="" aria-hidden="true"></i> Sign Up </a>
              </li>
              <li class="dropdown">
                <a ui-sref="login" class="navbar-link"><i class="" aria-hidden="true"></i> Log In</a>
              </li>
            </ul>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="restOfPage">
      <div class="main-container parent-sep">
        <div class="white-container" ui-view></div>
      </div>
      <!-- <div class="default-container">
        <div class="center-whitebox row" ui-view>
          stuff goes here
        </div>
      </div> -->
    </div>
</body>
<!-- Latest compiled and minified CSS -->
<!-- Latest compiled and minified JavaScript -->
<script src="assets/dependencies/lib/node_modules/jquery/dist/jquery.min.js"></script>
<script src="assets/dependencies/lib/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="assets/dependencies/lib/node_modules/angular/angular.min.js"></script>
<script src="assets/dependencies/lib/node_modules/satellizer/satellizer.min.js"></script>
<script src="assets/dependencies/lib/node_modules/angular-ui-router/release/angular-ui-router.min.js">
<script src="assets/dependencies/lib/etc/hover/hover.css">
</script>
<script src="assets/js/app.js"></script>
</html>