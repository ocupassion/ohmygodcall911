<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Carbon\Carbon;

class courseTest extends TestCase
{
    /* Useful methods
            
        Set the DB to ignore foreign key restraints
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        and to undo
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Empty tables
        DB::table('example')->truncate(); 
    */

    public function setUp()
    {
        parent::setUp();

        DB::table('course_user')->truncate(); 
        DB::table('users')->truncate(); 

        $newuser = [
            'name' => "rhys",
            'email' => "rhyslovesboys@gmail.com",
            'password' => Hash::make("fatherkamilaschildren"), 
        ];

        $this->user = App\User::create($newuser);

        Auth::loginUsingId($this->user->id);

        DB::table('institutions')->truncate();

        $newinstitution = [
            'name' => 'alacrityuk',
            'subdomain' => 'alacrityuk',
            'logo_path' => 'a',
            'banner_path' => 'b',
        ];

        $this->institution = App\Institution::create($newinstitution);

        Portal::setInstitution($this->institution);

        DB::table('courses')->truncate();
        DB::table('course_institution')->truncate();


        $newCourse = [
            'title' => "Starting Your Own Business",
            'description' => "Here you will find what starting your own business means, how to take an idea from the start, identify customers, find the ultimate users and create a product that you can sell.",
            'heading' => "Turn an idea into a business",
            'badge' => "/assets/img/courses/Startingyourownbusiness/badge.png",
            'banner' => "/assets/img/courses/Startingyourownbusiness/banner.png",
            'color' => '#60bddb'
        ];

        $this->course = App\Course::create($newCourse);

    }
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testEnrolUserOnCourse()
    {
        //Test the enrolling of a user

        $courseController = new App\Http\Controllers\CourseController;

        // Test
        //
        // Given a invalid course -> 404
        // Given a course that is not part of institution -> 404
        // Given a course the user has not taken -> 200
        // Given a course the user has taken but not completed -> ????
        // Given a course the user has completed -> 200

        $response = $courseController->enrolUserOnCourse('qdq');

        $this->assertTrue($response->status() == 404, 'Holy molly this should be a 404 as the institution cant have a course that is not valid');
        //

        $response = $courseController->enrolUserOnCourse($this->course->id);

        $this->assertTrue($response->status() == 404, 'Holy molly this should be a 404 as the course does not belong to this course');

        //

        $this->institution->courses()->attach($this->course);

        $response = $courseController->enrolUserOnCourse($this->course->id);

        $this->assertTrue($response->status() == 200, 'This should ernol the user as the course belongs to the institution and the user has not already started this course.');

        //

        $response = $courseController->enrolUserOnCourse($this->course->id);

        $this->assertTrue($response->status() == 404, "User has a not completed the course so don't allow them to create another record");

        //

        $this->user->courses()->updateExistingPivot($this->course->id, ['completed_at'=> Carbon::now()]);

        $response = $courseController->enrolUserOnCourse($this->course->id);

        $this->assertTrue($response->status() == 200, 'This should enrol the user as the course belongs to the institution and the user has finished the previous record.');

    }

    public function tearDown()
    {
        parent::tearDown();
    }
}