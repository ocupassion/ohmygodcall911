<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubDomainExtractionTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testgetSubDomain()
    {
        // Test getSubDomain($url) in SubDomainExtraction middleware

        $middleware = new App\Http\Middleware\SubDomainExtraction;
        
        // Test
        // - given null value -> null
        // - Given a invalid URL -> null
        // - Given valid URL without sub domains -> null
        // - Given valid URL with single sub domains -> array of sub
        // - Given valid url with more than one sub domian -> array of subs

        $this->assertTrue($middleware->getSubdomain(null) == null, 'If I pass an empty url string then i should get a null back');
        //

        $this->assertTrue($middleware->getSubdomain('oisjojs')==null,'If I
            pass a invalid URL it should get null back');
        //

        $this->assertTrue($middleware->getSubdomain('http://www.itc.com')==
          null,'If I pass a valid URL without sub domains it will get null');
        //

        $result = $middleware->getSubdomain('http://www.test.occupassion.co.uk');

        $this->assertTrue(in_array('test', $result), 'Result should contain test in an array');

        //

        $result = $middleware->getSubdomain('http://www.test.act.occupassion.co.uk');

        $this->assertTrue(in_array('test', $result) && in_array('act', $result), 'Result should contain test in an array');

    }

    public function testvalidateSubDomain(){
        // Test an array of sub doamains to see if they are valid

        $middleware = new App\Http\Middleware\SubDomainExtraction;

        DB::table('institutions')->truncate();

        // Test data
        $newInstitution = [
            'name' => 'testcenter',
            'subdomain' => 'testcentersub',
            'logo_path' => 'testlogopath',
            'banner_path' => 'testbannerpath'
        ];

        App\Institution::create($newInstitution);

        // Test
        // Giving an empty array -> Null
        // Giving a string -> Null
        // Subdomain that doesnt exist -> Null
        // Subdomain that exists -> App/Institution
        // Subdomain that exists but isnt first in array -> Null

        $this->assertTrue($middleware->validateSubDomains([]) == null, 'This is an empty array, should result in Null institution');

        //

        $this->assertTrue($middleware->validateSubDomains('testcentersub') == null, 'This is a string, should result in NULL Institution');

        //

        $this->assertTrue($middleware->validateSubDomains(['alacrityuk']) == null, 'This is a not a valid institution name, should result in null institution');

        //
        $institution = $middleware->validateSubDomains(['testcentersub']);

        $this->assertTrue($institution != null, 'This is a valid institution and should result institution returned');

        $this->assertTrue(is_a($institution, 'App\Institution'), 'A valid institution class should be returned');

        //

        $this->assertTrue($middleware->validateSubDomains(['lol', 'testcentersub']) == null, 'A valid institution is passed in but not as the first element in the array so it is not found.');
    }
}
