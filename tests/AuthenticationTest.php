<?php

use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthenticationTest extends TestCase
{   
    public function setUp()
    {
        parent::setUp();
        // Test Data:
        // Clear user table

        DB::table('institution_user')->truncate();
        DB::table('users')->truncate();

        // Create a user  
        $newUser = [
            'name' => 'Ben',
            'email' => 'ben@occupassion.co.uk',
            'password' => Hash::make('ben1234'),
        ];

        $this->user = App\User::create($newUser);

        // make default Request to pass into function
        $this->request = new Request;

        // Get controller
        $this->controller = new App\Http\Controllers\AuthenticateController;

        DB::table('institutions')->truncate();

        $newInstitution =  [
            'name' => 'test', 
            'subdomain' => 'subdomain-test',
            'logo_path' => 'ss', 
            'banner_path'=> 'rr',
        ];

        $this->institution = App\Institution::create($newInstitution);
        Portal::setInstitution($this->institution);
    } 
    /**
     * Test the Authenticate function inside the Authenticate Controller.
     *
     * @return void
     */
    public function testAuthenticate()
    {
        // Test retriving a token from the Authenticate(Request $this->request)

        
        // Test
        // Request without Email or password -> 401 unauthorized
        // Request with invalid email and invalid password -> 
        // Request with incorrect email
        // Request with incorrect password -> 
        // Given a user who does not belong to the institution but has correct details ->
        // 403
        // Give the user belong  to a institution and user details are correct -> user
        //User belongs to an institution but not the one being accessed - user details are correct -> 403, forbidden

        $this->request->replace([]);

        $response = $this->controller->authenticate($this->request);

        $this->assertTrue($response->status() == 401, 'This response should say unauthorized as the details given (none) are invalid');

        //
        $this->request->replace(['email' => 'testbad', 'password' => 'Testbad2']);

        $response = $this->controller->authenticate($this->request);

        $this->assertTrue($response->status() == 401, 'This response should say unauthorized as the details given (random string) are invalid');

        //

        $this->request->replace(['email' => 'rhys@occupassion.co.uk', 'password' => 'Test']);

        $response = $this->controller->authenticate($this->request);

        $this->assertTrue($response->status() == 401, 'This response should say unauthorized as the details given (incorrect email) are invalid');

        //

        $this->request->replace(['email' => 'ben@occupassion.co.uk', 'password' => 'Test']);

        $response = $this->controller->authenticate($this->request);

        $this->assertTrue($response->status() == 401, 'This response should say unauthorized as the details given (incorrect password) are invalid');
        //

        $this->request->replace(['email' => 'ben@occupassion.co.uk', 'password' => 'ben1234']);

        $response = $this->controller->authenticate($this->request);
        $this->assertTrue($response->status() == 403,"this should be a 403, the user don't belong");

        //

        $this->user->institutions()->attach($this->institution->id);

        $this->request->replace(['email' => 'ben@occupassion.co.uk', 'password' => 'ben1234']);

        $response = $this->controller->authenticate($this->request);

        $this->assertTrue($response->status() == 200, 'This response be a status 200 as its the correct user details were sent and the user belongs to this institution');

        $this->assertContains('token', $response->content(), 'damn i should have a token for ');

        //Setup new institution

        $newInstitution =  [
            'name' => 'celtic', 
            'subdomain' => 'celtic',
            'logo_path' => 'ss', 
            'banner_path'=> 'rr',
        ];

        $institution = App\Institution::create($newInstitution);
        Portal::setInstitution($institution);

        $response = $this->controller->authenticate($this->request);
        $this->assertTrue($response->status() == 403, 'I dont belong to this portal, I should be forbidden.');
    }
}
