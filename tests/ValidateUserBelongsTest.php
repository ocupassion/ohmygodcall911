<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ValidateUserBelongsTest extends TestCase
{   
    /* Useful methods
            
        Set the DB to ignore foreign key restraints
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        and to undo
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Empty tables
        DB::table('example')->truncate(); 
    */
        
    
    /**
     * A basic functional test to check the middleware to see if the user belongs.
     *
     * @return void
     */
    public function testValidateUserBelongsMiddleware()
    {
        $middleware = new App\Http\Middleware\ValidateUserBelongs;
        
        //Default request - with no data
        $request = new Request;
        
        $next = function(){
            return 'Well done bruv';
        };

        DB::table('users')->truncate(); 

        $newuser = [
            'name' => "rhys",
            'email' => "rhyslovesboys@gmail.com",
            'password' => Hash::make("fatherkamilaschildren"), 
        ];

        $user = App\User::create($newuser);

        DB::table('institutions')->truncate();

        $newinstitution = [
            'name' => 'alacrityuk',
            'subdomain' => 'alacrityuk',
            'logo_path' => 'a',
            'banner_path' => 'b',
        ];

        $institution = App\Institution::create($newinstitution);

        Portal::setInstitution($institution);

        DB::table('institution_user')->truncate();


        // Test
        // User is not currently signed in/no token provided -> 401 
        // The user is valid but is not part of the institution -> 403
        // User is valid and belongs to the institution -> $next()

        $response = $middleware->handle($request, $next);
        
        $this->assertTrue($response->status() == 401, 'This response should say unauthorized because no user was given');

        //

        Auth::loginUsingId($user->id);

        $response = $middleware->handle($request, $next);

        $this->assertTrue($response->status() == 403, 'This response should say forbidden as they do not have access to the institution');
        
        //User is valid
        
        // Make the user belong to institution 

        $user->institutions()->attach($institution->id);

        $response = $middleware->handle($request, $next);
        $this->assertTrue($response == 'Well done bruv','error');
    }
}
