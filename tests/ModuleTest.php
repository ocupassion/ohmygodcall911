<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Carbon\Carbon;

class ModuleTest extends TestCase
{
    /* Useful methods
            
        Set the DB to ignore foreign key restraints
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        and to undo
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Empty tables
        DB::table('example')->truncate(); 
    */

    public function setUp()
    {
        parent::setUp();

        DB::table('courses')->truncate();
        DB::table('modules')->truncate();
        DB::table('institutions')->truncate();
        DB::table('users')->truncate(); 
        DB::table('course_institution')->truncate();
        DB::table('course_user')->truncate();
        DB::table('module_user')->truncate();


        $newCourse = [
            'title' => "Starting Your Own Business",
            'description' => "Here you will find what starting your own business means, how to take an idea from the start, identify customers, find the ultimate users and create a product that you can sell.",
            'heading' => "Turn an idea into a business",
            'badge' => "/assets/img/courses/Startingyourownbusiness/badge.png",
            'banner' => "/assets/img/courses/Startingyourownbusiness/banner.png",
            'color' => '#60bddb'
        ];

        $this->course = App\Course::create($newCourse);

        $newModule = [
            'title' => "What do you know?",
            'description' => "Pre question to validate their knowledge",
            'order' => 1,
            'resource' => null
        ];
        $this->module = App\Module::create($newModule);

        $newinstitution = [
            'name' => 'alacrityuk',
            'subdomain' => 'alacrityuk',
            'logo_path' => 'a',
            'banner_path' => 'b',
        ];

        $this->institution = App\Institution::create($newinstitution);

        Portal::setInstitution($this->institution);

        $newuser = [
            'name' => "rhys",
            'email' => "rhyslovesboys@gmail.com",
            'password' => Hash::make("fatherkamilaschildren"), 
        ];

        $this->user = App\User::create($newuser);

        Auth::loginUsingId($this->user->id);
    }
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testCheckBelongsCourse()
    {
        //Test the check if module belongs too a course based on id

        // Test
        //
        // Given a invalid course id -> false
        // Given a valid course id that its does not belong too -> false
        // Given a course id the module belongs too -> true

        $this->assertTrue($this->module->checkBelongsCourse('lol') == false, 'Invalid course id should result with a false module does belong to course');

        //

        $this->assertTrue($this->module->checkBelongsCourse($this->course->id) == false, 'Course id should result with a false module does belong to course');

        //

        $this->module->course_id = $this->course->id;
        $this->module->save();

        $this->assertTrue($this->module->checkBelongsCourse($this->course->id) == true, 'Course id should result with a false module does belong to course');
    }

    public function testUserStartsModule(){

        $moduleController = new App\Http\Controllers\ModuleController;

        //Test user starting a module
        // Test
        //
        // Given a invalid course id and module id -> 404
        // Given a course id that is not part of institution-> 404
        // Given a module is that is not part of course-> 404
        // Given a valid course id is part of institution and the module id belongs to but the user is not currently taking -> 400;
        // Given a valid course id is part of institution and the module id belongs to but the user is taking -> 200;


        $response = $moduleController->userStartsModule('', '');
        $this->assertTrue($response->status() == 404, 'Given a invalid course id and module id -> 404');

        //

        $response = $moduleController->userStartsModule($this->course->id, $this->module->id);
        $this->assertTrue($response->status() == 404, 'Given a course id that is not part of institution-> 404');

        //

        $this->institution->courses()->attach($this->course->id);

        $response = $moduleController->userStartsModule($this->course->id, $this->module->id);
        $this->assertTrue($response->status() == 404, 'Given a module is that is not part of course');

        //

        $this->module->course_id = $this->course->id;
        $this->module->save();

        $response = $moduleController->userStartsModule($this->course->id, $this->module->id);

        $this->assertTrue($response->status() == 400, 'User has not taken the course so this should result in a 400');
        //

        $this->user->courses()->attach($this->course);

        $response = $moduleController->userStartsModule($this->course->id, $this->module->id);
        $this->assertTrue($response->status() == 200, 'User has taken module');
        //

        $response = $moduleController->userStartsModule($this->course->id, $this->module->id);
        $this->assertTrue($response->status() == 400, 'User has not completed the module');

    }

    public function tearDown()
    {
        parent::tearDown();
    }
}