<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    use WithoutMiddleware;
    
    /* Useful methods
            
        Set the DB to ignore foreign key restraints
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        and to undo
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Empty tables
        DB::table('example')->truncate(); 
    */
        
    public function setUp()
    {
        parent::setUp();
    }
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('josh');
    }
}
