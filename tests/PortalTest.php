<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PortalTest extends TestCase
{
    
    /* Useful methods
            
        Set the DB to ignore foreign key restraints
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        and to undo
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Empty tables
        DB::table('Institution')->truncate(); 
    */
        
    public function setUp()
    {
        parent::setUp();

        DB::table('institutions')->truncate();

        $newInstitution = ['name'=>'Alacrity','subdomain' =>'SubAlacrity', 'logo_path'=>'assets/img/logo/alacrity.png','banner_path'=>'banner'];

        $this->institution = App\Institution::create($newInstitution);

    }
    /**
     * A basic functional test Institution.
     *
     * @return void
     */
    public function testGetInstitutionLogo()
    {

        $url = route('getPortalLogo');
        $main_domain = env('SUBDOMAIN_MAIN_DOMAIN');
        $domains = $this->institution->subdomain . '.' . $main_domain;

        $url = str_replace($main_domain, $domains, $url);

        //Test
        //Call get institution logo -> url

        $response = $this->call('GET',$url);
        
        $this->assertTrue($response->content()== $this->institution->logo_path);
    }

    /**
     * A basic functional test Institution.
     *
     * @return void
     */
    public function testGetInstitutionBanner()
    {

        $url = route('getPortalBanner');
        $main_domain = env('SUBDOMAIN_MAIN_DOMAIN');
        $domains = $this->institution->subdomain . '.' . $main_domain;

        $url = str_replace($main_domain, $domains, $url);

        //Test
        //Call get institution logo -> url

        $response = $this->call('GET',$url);
        
        $this->assertTrue($response->content()== $this->institution->banner_path);
    }
}
