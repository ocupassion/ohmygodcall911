<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Carbon\Carbon;

class UserTest extends TestCase
{
    /* Useful methods
            
        Set the DB to ignore foreign key restraints
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        and to undo
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Empty tables
        DB::table('example')->truncate(); 
    */

    public function setUp()
    {
        parent::setUp();

        DB::table('course_user')->truncate(); 
        DB::table('users')->truncate(); 

        $newuser = [
            'name' => "rhys",
            'email' => "rhyslovesboys@gmail.com",
            'password' => Hash::make("fatherkamilaschildren"), 
        ];

        $this->user = App\User::create($newuser);

        DB::table('courses')->truncate();

        $newCourse = [
            'title' => "Starting Your Own Business",
            'description' => "Here you will find what starting your own business means, how to take an idea from the start, identify customers, find the ultimate users and create a product that you can sell.",
            'heading' => "Turn an idea into a business",
            'badge' => "/assets/img/courses/Startingyourownbusiness/badge.png",
            'banner' => "/assets/img/courses/Startingyourownbusiness/banner.png",
            'color' => '#60bddb'
        ];

        $this->course = App\Course::create($newCourse);

    }
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testCheckCourseNotCompleted()
    {
        //Test the check if course is not complete

        // Test
        //
        // Given a invalid course -> false
        // Given a course the user has not taken -> false
        // Given a course the user has taken but not completed -> true
        // Given a course the user has completed -> false

        $this->assertTrue($this->user->CheckCourseNotCompleted(1000) == false, "The course is invalid so assume completed courses should be found");

        //

        $this->assertTrue($this->user->CheckCourseNotCompleted($this->course->id) == false, "The user has not taken the course so assume completed courses should be found");

        //

        $this->user->courses()->attach($this->course);

        $this->assertTrue($this->user->CheckCourseNotCompleted($this->course->id) == true, "The user has taken the course but has completed the course");

        //


        $this->user->courses()->updateExistingPivot($this->course->id, ['completed_at'=> Carbon::now()]);

        $this->assertTrue($this->user->CheckCourseNotCompleted($this->course->id) == false, "The user has taken the course and has completed the course");
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}