<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
        public static $BELONGS = ['jwt.auth', 'auth.belongs'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'lastname', 'profile_url',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
    *  Check is user has any open records for course
    *
    *   @param course id
    *   @return boolean
    */
    public function checkCourseNotCompleted($course_id){
        if($this->courses()->whereCourseId($course_id)->count()){
            return $this->courses()->whereCourseId($course_id)->whereCompletedAt('0000-00-00 00:00:00')->count();
        }else{
            return 0;
        }
    }

    /*
    *  Check is user has any open records for module
    *
    *   @param course id
    *   @return boolean
    */
    public function checkModuleNotCompleted($module_id){
        if($this->modules()->whereModuleId($module_id)->count()){
            return $this->modules()->whereModuleId($module_id)->whereCompletedAt('0000-00-00 00:00:00')->count();
        }else{
            return 0;
        }
    }

    /**
     * The institutions that belong to the user.
     */
    public function institutions()
    {
        return $this->belongsToMany('App\Institution')->withTimestamps();
    }

     /**
     * The courses belong to a user.
     */
    public function courses()
    {
        return $this->belongsToMany('App\Course')->withPivot('id', 'started_at', 'completed_at');
    }

    /**
     * Many-To-Many Relationship Method for accessing the Module->Users
     *
     * @return QueryBuilder Object
     */
    public function modules()
    {
        return $this->belongsToMany('App\Module')->withPivot('started_at','completed_at','course_user_id');
    }
}
