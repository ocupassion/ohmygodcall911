<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['title', 'name', 'description', 'course_id', 'order', 'resource'];

    /**
     * Check module belongs to course based on course id
     *
     * @return boolean
     */
    public function checkBelongsCourse($course_id){
        return $this->course()->whereId($course_id)->count();
    }

    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Many-To-Many Relationship Method for accessing the Module->Courses
     *
     * @return QueryBuilder Object
     */
    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    /**
     * Many-To-Many Relationship Method for accessing the Module->Users
     *
     * @return QueryBuilder Object
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('started_at','completed_at','course_user_id');
    }
}