<?php
namespace App\Packages\Portal\src\Facades;

use Illuminate\Support\Facades\Facade;

class Portal extends Facade {
   protected static function getFacadeAccessor() { return 'Portal'; }
}