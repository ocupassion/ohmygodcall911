<?php

namespace App\Packages\Portal\src;

class Portal
{
    /**
    * value
    *
    * @var Institution model
    */
   protected $institution = null;

   /**
    * @param string  $value
    */
   public function __construct()
   {
   
   }

   public function setInstitution($value)
   {
       $this->institution = $value;
   }

   public function getInstitution()
   {
       return $this->institution;
   }
}