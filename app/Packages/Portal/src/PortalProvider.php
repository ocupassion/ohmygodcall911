<?php

namespace App\Packages\Portal\src;

use Illuminate\Support\ServiceProvider;

class PortalProvider extends ServiceProvider
{
   /**
    * Register bindings in the container.
    *
    * @return void
    */
   public function register()
   {
       $this->app->singleton('Portal', function () {
           return new Portal();
       });
   }
}