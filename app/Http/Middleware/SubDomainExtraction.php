<?php

namespace App\Http\Middleware;
use App\Institution;
use App\Packages\Portal\src\Facades\Portal;

// THis middleware extracts the subdomain from the url
class SubDomainExtraction {
	
	public function handle($request, $next) 
	{
		// Get all the sub domains
		$arrayOfSubDomains = $this->getSubdomain($request->url());

		// If valid sub domains
		
		$institution = $this->validateSubDomains($arrayOfSubDomains);
		
		// Set Portals institution
		Portal::setInstitution($institution);

		return $next($request);
	}

	public function validateSubDomains($arrayOfSubDomains)
	{ 
		if(!empty($arrayOfSubDomains) && is_array($arrayOfSubDomains))
		{	
			$institution = Institution::wheresubdomain($arrayOfSubDomains[0])->first();
		}
		else{
			$institution = null;
		}

		return $institution;
	}

	public function getSubdomain($url)
	{
		// Pull out the subdomain from the url

		// Splits the url into parts (hosts, port ect)
		$splitUrl = parse_url($url);

		empty($splitUrl['host'])	
			? $domains = null
			: $domains = str_replace(['www.', '.com', '.co.uk'], '', $splitUrl['host']);

		// Remove Main Domain
		$ignore = env('SUBDOMAIN_MAIN_DOMAIN');
		
		$domains = str_replace($ignore, '', $domains);

		// Split host into individual domains
		empty($domains) 
			? $result = null
			: $result = explode('.', $domains, -1);

		return $result;
	}
}