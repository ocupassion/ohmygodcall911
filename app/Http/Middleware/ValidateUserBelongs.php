<?php

namespace App\Http\Middleware;
use App\Institution;
use App\Packages\Portal\src\Facades\Portal;
use Auth;
use Illuminate\Http\Request;


// Validates that the user belongs to the portal

class validateUserBelongs {
	
	public function handle($request, $next) 
	{
		// Check the user already exists 
		if(!Auth::user()){
			return response()->json(['error'=>'Unauthorized'], 401);
		}

		// Check if the user belongs to the portal
		Auth::user()->Institutions()->whereInstitutionId(Portal::getInstitution()->id)->count()
		? $response = $next($request)
		: $response = response()->json(['error'=>'Forbidden'], 403);
		return $response;
	}
}