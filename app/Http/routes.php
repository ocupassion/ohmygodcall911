<?php

use App\Packages\Portal\src\Facades\Portal;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
	Routes for returning landing page or portal
*/

Route::get('/', function () {
	if(Portal::getInstitution())
    {	
    	return view('index');
    }

    return 'Why am i not a landing page huh Josh!';
});

/*
	All API calls for posting and getting data from the sever
*/

Route::group(['prefix' => 'api'], function()
{
	/*
		Log in to get users token
	*/
    Route::post('login', [
    	'uses' => 'AuthenticateController@authenticate',
    	'as' => 'login',
    ]);

    Route::get('get/portal/logo',[
        'uses' => 'PortalController@getLogo',
        'as' => 'getPortalLogo'
    ]);

    Route::get('get/portal/banner',[
        'uses' => 'PortalController@getBanner',
        'as' => 'getPortalBanner'
    ]);

    Route::group(['middleware' => App\User::$BELONGS], function()
    {
        /*
         *************************
         ***All Course routes***
         *************************
         */
        Route::get('courses', [
            'uses' => 'CourseController@getCourses',
            'as' => 'getCourses',
        ]);

        Route::get('course/{course_id}', [
            'uses' => 'CourseController@getCourse',
            'as' => 'getCourse',
        ]);

        Route::get('course/{course_id}/modules', [
            'uses' => 'CourseController@getCourseModules',
            'as' => 'getCourseModules',
        ]);

        Route::post('course/{course_id}/enrol', [
            'uses' => 'CourseController@enrolUserOnCourse',
            'as' => 'enrolUserOnCourse',
        ]);

        //User routes
        Route::get('get/user',[
            'uses' => 'UserController@get',
            'as' => 'getUser'
        ]);

        //Module route
        Route::post('course/{course_id}/module/{module_id}/start',[
            'uses' => 'ModuleController@userStartsModule',
            'as' => 'userStartsModule'
        ]);
    });
});



