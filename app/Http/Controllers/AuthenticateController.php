<?php

namespace App\Http\Controllers;

use App\Packages\Portal\src\Facades\Portal;
use Illuminate\Http\Request;
use JWTAuth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;

class AuthenticateController extends Controller
{
    public function authenticate(Request $request)
    {   
        $credentials = $request->only('email', 'password');
 
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $this->belongsToInstitution()
            ? $response = response()->json(compact('token'))
            : $response = response()->json(['error' => 'Forbidden'], 403);
        
        // if no errors are encountered we can return a JWT
        return $response;
    }
    public function belongsToInstitution()
    {
        return Auth::user()->institutions()->whereInstitutionId(Portal::getInstitution()->id)->count();
    }
}