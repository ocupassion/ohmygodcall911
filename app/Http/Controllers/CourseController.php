<?php

namespace App\Http\Controllers;

use App\Packages\Portal\src\Facades\Portal;
use Illuminate\Http\Request;
use App\Course;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class CourseController extends Controller
{
    /**
     * Returns all courses that are suitable for this institution
     *
     * @return Collection
     */
    public function getCourses()
    {
        $institution = Portal::getInstitution();
        $courses = $institution->courses;
        return !$courses->isEmpty()
            ? $courses
            : response()->json(['error' => 'No courses found for institution'], 404);;
    }

    /**
     * Return a course based the given ID if suitable for this institution
     * 
     * @param CourseId integer 
     * @return App\Course
     */
    public function getCourse($course_id)
    {
        $institution = Portal::getInstitution();
        $course = $institution->courses()->find($course_id);
        return $course
            ? $course
            : response()->json(['error' => 'Course not found for institution'], 404);
    }

    /**
     * Return a courses modules if course is suitable for this institution
     *
     * @return App\Course
     */
    public function getCourseModules($course_id)
    {
        $institution = Portal::getInstitution();
        $course = $institution->courses()->find($course_id);
        
        $course
        ? $modules = $course->modules
        : $modules = collect([]);

        return !$modules->isEmpty()
            ? $modules
            : response()->json(['error' => 'Course modules not found'], 404);
    }

    /**
     * Enrols a user on a course
     *
     */
    public function enrolUserOnCourse($course_id)
    {   
        // Error check
        $response = null;

        if(Auth::user()->checkCourseNotCompleted($course_id)){
            $response = response()->json(['error'=>'User has not completed course'], 404);
        }

        if($response == null && Portal::getInstitution()->courses()->whereCourseId($course_id)->count() < 1){
            $response = response()->json(['error'=>'Course not found for institution'], 404);
        }
        
        if($response == null){
            Auth::User()->courses()->attach($course_id);
            $response = response()->json(['message' => 'User enrolled successfully'], 200);
        }

        return $response;
    }
}