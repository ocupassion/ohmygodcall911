<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Http\Requests;

class UserController extends Controller
{
	/**
     * Return user from token
     *
     * @return User
     */
    public function get()
    {
    	return Auth::user();
    }
}
