<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Packages\Portal\src\Facades\Portal;


class ModuleController extends Controller
{
    /**
     *  user starts a module
     *
     */
    public function userStartsModule($course_id, $module_id)
    {   
    	$response = $this->validateCourseIdandModuleId($course_id, $module_id);
    	if($response == null){
			Auth::user()->modules()->attach($module_id, ['course_user_id' => Auth::user()->courses()->whereCourseId($course_id)->first()->pivot->id]);
			$response = response()->json(['message' => 'User starts module'], 200);
		}

		return $response;
    }

    public function validateCourseIdandModuleId($course_id, $module_id){
    	
    	$response = null;

    	//Does the course belong to this portal
    	$course = Portal::getInstitution()->courses()->whereCourseId($course_id)->first();

    	if(!$course){
    		$response = response()->json(['error' => 'Course not found for institution'],404); 
    	}else{
            // Does the module belong to this course
    		if(!$course->modules()->whereId($module_id)->count()){
    			$response = response()->json(['error' => 'Module not found for course'], 404);
    		}

            if($response == null){
                if(!Auth::user()->checkCourseNotCompleted($course_id)){
                    $response = response()->json(['error' => 'User has not got  course in progress'], 400);
                }
            }

            if($response == null){
                if(Auth::user()->checkModuleNotCompleted($module_id)){
                    $response = response()->json(['error' => 'User has module in progress'], 400);
                }
            }
    	}
    	
    	return $response;
    }
}
