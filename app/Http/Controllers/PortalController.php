<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Packages\Portal\src\Facades\Portal;

use App\Http\Requests;

class PortalController extends Controller
{
    public function getLogo()
    {
    	return Portal::getInstitution()->logo_path;
    }

    public function getBanner()
    {
    	return Portal::getInstitution()->banner_path;
    }
}
