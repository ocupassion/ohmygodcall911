<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'subdomain', 'logo_path', 'banner_path',
    ];

    public function checkCourseBelongs($course_id){
        return $this->courses()->whereCourseId($course_id)->count();
    }

    /**
     * The users that belong to the institution.
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function courses()
    {
        return $this->belongsToMany('App\Course')->withTimestamps();
    }
}
