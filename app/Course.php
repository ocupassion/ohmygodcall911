<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Auth;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'name', 'description', 'badge', 'banner', 'heading', 'color'];

    protected $appends = array('user_in_progress');


    /*
     * Add User taken flag
     *
     */
    public function getUserInProgressAttribute()
    {
        return Auth::user()->checkCourseNotCompleted($this->id);
    }

    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Many-To-Many Relationship Method for accessing the course->modules
     *
     * @return HasMany
     */
    public function modules()
    {
        return $this->hasMany('App\Module')->orderBy('order');;
    }

    /**
     * Many-To-Many Relationship Method for accessing the course->Users
     *
     * @return QueryBuilder Object
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function institutions()
    {
        return $this->belongsToMany('App\Institution')->withTimestamps();
    }

}
