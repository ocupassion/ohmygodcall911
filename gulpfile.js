var gulp  = require('gulp');
var ngAnnotate = require('gulp-ng-annotate');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var minifycss = require('gulp-minify-css');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync');
var neat = require('node-neat');
var reload = browserSync.reload;

var paths = {
 dest:'public/assets/',
 resources:'resources/assets/',
 jquery: './node_modules/jquery/',
 bootstrap: './node_modules/bootstrap-sass-official/assets/'
};

/* Scripts task */
gulp.task('scripts', function() {
 return gulp.src([
 /* Add your JS files here, they will be combined in this order */
   paths.resources + 'js/**/*.js' 
 ])
 .pipe(concat('app.js'))
 .pipe(plumber())
 .pipe(ngAnnotate())
 .pipe(gulp.dest(paths.dest + 'js'))
 .pipe(rename({suffix: '.min'}))
 .pipe(uglify())
 .pipe(gulp.dest(paths.dest + 'js'));
});    

/* Sass task */
gulp.task('sass', function () {  
 gulp.src(paths.resources +'sass/app.scss')
 .pipe(plumber())
 .pipe(sass({
   includePaths: ['scss'].concat(neat)
 }))
 .pipe(gulp.dest(paths.dest + 'css'))
 .pipe(rename({suffix: '.min'}))
 .pipe(minifycss())
 .pipe(gulp.dest(paths.dest + 'css'))
 /* Reload the browser CSS after every change */
 .pipe(reload({stream:true}));
});

/* Reload task */
gulp.task('bs-reload', function () {
 browserSync.reload();
});

/* Prepare Browser-sync for localhost */
gulp.task('browser-sync', function() {
 browserSync.init([paths.dest + 'css/*.css', paths.dest +'js/**.js'], {
   proxy: 'localhost:8000'
 });
});

/* Watch scss, js and html files, doing different things with each. */
gulp.task('default', ['sass', 'scripts', 'browser-sync'], function () {
 /* Watch scss, run the sass task on change. */
 gulp.watch([paths.resources + 'sass/*.scss',paths.resources + 'sass/**/*.scss'], ['sass']);
 /* Watch app.js file, run the scripts task on change. */
 gulp.watch([paths.resources  + 'js/**/*.js'], ['scripts']);
 /* Watch .html files, run the bs-reload task on change. */
 gulp.watch(['resources/views/*.php','public/assets/views/**/**/**/*.html'], ['bs-reload']);
});