angular.module("occupassion",['satellizer','ui.router'])
.config(["$stateProvider", "$authProvider", "$urlRouterProvider", function($stateProvider,$authProvider,$urlRouterProvider){

	// The login url
	$authProvider.loginUrl = '/api/login';

	// Default state 
  $urlRouterProvider.otherwise('/login');

	// URL based nav system
	$stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'assets/views/auth/login.html',
        controller: 'LogInCtrl',
        controllerAs: 'loginScope',
        authenticate: false
      })
      .state('courses', {
        url: '/courses',
        templateUrl: 'assets/views/main/courses.html',
        controller: 'CoursesCtrl',
        controllerAs: 'coursesScope',
        authenticate: true
      })
      .state('course',{
        url:'/course/{courseId}/',
        templateUrl: 'assets/views/main/course.html',
        controller: 'CourseCtrl',
        controllerAs: 'courseScope',
        authenticate: true
      })
      .state('content',{
        url:'/course/{courseId}/module/{moduleId}',
        templateUrl: 'assets/views/main/content.html',
        controller: 'ContentCtrl',
        controllerAs: 'contentScope',
        authenticate: true
      })
}])
.run(["$rootScope", "$state", "$stateParams", "authService", function($rootScope, $state, $stateParams, authService) {
	
	$rootScope.$state = $state;

  $rootScope.$on('$stateChangeStart', function(event, toState) {

      // Try and get a user again from token if i don't have one in scope
      if(!$rootScope.user && toState.authenticate){
        if(localStorage.getItem('satellizer_token') != null){
  		    gotTokenNoUser();
        }else{
          redirectLogin();
        }
      }

      function gotTokenNoUser(){ 
        // Go get me my user details
        authService.getUser()
        .then(function(response){
          if(!$rootScope.user){
            redirectLogin();
          }
        });
      }

      function redirectLogin(){
        // Preventing the default behavior allows us to use $state.go
        // to change states
        event.preventDefault();

        $state.go('login');
      }
  });
}]);


'use strict';
var app = angular.module("occupassion");

app.controller("CourseCtrl",CourseCtrl);

// Inject dependencies
CourseCtrl.$inject = ['$scope', '$rootScope', 'courseService', '$state'];

function CourseCtrl($scope, $rootScope, courseService, $state){

    var courseController = this;

	if(!courseService.courseData){
	    courseService.getCourse().then(function(response){
	    	courseController.course = courseService.courseData;
	    });
	}else{
		courseController.course = courseService.courseData;
	}

	courseService.getModules()
	.then(function(response){
		courseController.modules = courseService.courseModules;
	});

	courseController.enrolCourse = function(){
		courseService.enrolCourse();
	}

	courseController.startModule = function(courseId, moduleId){
		courseService.startModule(moduleId)
		.then(function(response){
			if(!response.data.error){
				$state.go('content', {'courseId':courseId, 'moduleId':moduleId});
			}
		})
	}
}

'use strict'
angular
  .module('occupassion')
  .factory('courseService', courseService)

  courseService.$inject=['$http', '$rootScope', '$stateParams'];

  function courseService($http, $rootScope, $stateParams){
    return{
      courseData: null,
      courseModules: null,
      getCourse:getCourse,
      getCourses:getCourses,
      getModules:getModules,
      enrolCourse:enrolCourse,
      startModule:startModule,
    }

    // Public functions
    function getCourses(){
      return $http.get('/api/courses')
        .then(function (response) {
          return response.data;
        }, function(response){
          console.log(response.data.error);
        });
    }

    function getCourse(){
      self = this;

      // If course is not set attempt to get course
      return callGetCourse($stateParams.courseId)
      .then(function(response){
        self.courseData = response;
        return response;
      });
    }

    function getModules(){
      self = this;
      self.courseModules = null;
      return $http.get('api/course/' + $stateParams.courseId + '/modules')
      .then(function(response) {
        self.courseModules = response.data;
        return response.data;
      }, function(response){
        console.log(response.data.error);
        return response;
      })
    }

    function enrolCourse(){
      var self = this;
      return $http.post('api/course/' + $stateParams.courseId + '/enrol', {})
        .then(function (response) {
          self.courseData.user_in_progress = 1;
          return response;
        }, function(response){
          console.log(response.data.error);
        });
    }

    function startModule(moduleId){
      var self = this;
      return $http.post('api/course/' + $stateParams.courseId + '/module/' + moduleId +'/start', {})
        .then(function (response) {
          return response;
        }, function(response){
          console.log(response.data.error);
          return response;
        });

    }

    // Private functions
    function callGetCourse(courseId){
      // Get course
      return $http.get('/api/course/' + courseId)
        .then(function (response) {
          return response.data;
        }, function(response){
          console.log(response.data.error);
          return response;
        });
    } 
  };
  



'use strict';
var app = angular.module("occupassion");

app.controller("ContentCtrl",ContentCtrl);

// Inject dependencies
ContentCtrl.$inject = ['$scope', '$rootScope', 'courseService'];

function ContentCtrl($scope, $rootScope, courseService){

    var ContentController = this;
}

'use strict';
var app = angular.module("occupassion");

app.controller("CoursesCtrl",CoursesCtrl);

// Inject dependencies
CoursesCtrl.$inject = ['$scope', '$http', '$state', 'courseService'];

function CoursesCtrl($scope, $http, $state, courseService){

    var courseController = this;

	// this function communicates with the remote server, to
	// pull down all courses that are relevant for this user,
	// and the institution that they belong to
    courseService.getCourses()
        .then(function (courses) {
            courseController.courses = courses;
        });

    courseController.selectCourse = function(course) {
    	courseService.courseData = course;
    	$state.go('course', {'courseId': course.id});
    };
}
'use strict'
var app = angular.module("occupassion");

app.controller('FrameController', ["$scope", "$rootScope", "authService", "$state", function($scope,$rootScope,authService,$state) 
{

	var frame = this;

	frame.logout = function() {
		authService.logoutUser();
		$state.go('login');
	}
}])
angular
	.module('occupassion')
	.directive('alertMessage',alertMessage);
  function alertMessage(){
    var directive = {
			restric:'E',
			template:'<div class="{{alert.alertService.alertClass}}" ng-if="alert.alertService.showAlert"><p>{{alert.alertService.alertMessage}}</p></div>',
			controller:"AlertCtrl as alert",
			replace:true,
			bindToController:true
    };
    return directive;
  };

'use strict'
angular
  .module('occupassion')
  .factory('authService',authService)

  authService.$inject=['$http', '$auth', '$rootScope'];

  function authService($http, $auth, $rootScope){
    return{
      loginUser:loginUser,
      getUser:getUser,
      logoutUser:logoutUser
    }

    // Public functions
    
    function loginUser(email, password){
     
      // Users details 
      var credentials = {
        email: email,
        password: password
      }

      // Use the $auth.login to get user token 
      return $auth.login(credentials)
      .then(getUser, errorLog)
    }

    function getUser(response){
      return $http.get('api/get/user')
      .then(extractUser, errorLog);
    }

    function extractUser(response){
      // Set the current user
      $rootScope.user = response.data;
      return response;
    }

    function logoutUser(){
      // logout the user using the $auth.logout 
      // process (removes token)
      return $auth.logout()
      .then(removeUser)
    }

    // Private functions

    function removeUser(){
      // Remove user from rootScope
      $rootScope.user = null;
      return
    }

    function errorLog(error){
      // Return error response
      return error;
    }
  };
  



angular
	.module('occupassion')
	.factory('institutionService',institutionService);

	institutionService.$inject = ['$http'];

	function institutionService($http){
	 	return{
	 		getLogo:getLogo,
	 		getBanner:getBanner

	 	}

	 	function getLogo(){
	 		return $http.get('api/get/portal/logo')
	 		.then(function success(response){
	 			return response
	 		},function error(){
	 			console.log('Error')
	 		});
	 	}

	 	function getBanner(){
	 		return $http.get('api/get/portal/banner')
	 		.then(function success(response){
	 			return response
	 		},function error(){
	 			console.log('Error')
	 		});
	 	}
	 }


'use strict'
var app = angular.module("occupassion");

app.controller("LogInCtrl",LogInCtrl);

// Inject dependencies
LogInCtrl.$inject = ['$scope', '$http', 'authService', '$state','institutionService','alertService'];

function LogInCtrl($scope, $http, authService, $state, institutionService,alertService){
	var logIn = this;
	logIn.email = '';
	logIn.password = '';
	institutionService.getBanner().then(function(response){
		logIn.banner = response.data;

	});

	// Login functions
	logIn.login = function() {
		if(logIn.email != '' && logIn.password != ''){
			authService.loginUser(logIn.email, logIn.password)
			.then(function(response) {
				if(response.data.error){
					alertService.showAlert = true;
					alertService.alertMessage = response.data.error;
					alertService.alertClass = 'alert-message';
				}else{
					logIn.errorMessages = null;
					$state.go('courses');
				}
			});
		}
	}
}
angular
	.module('occupassion')
	.controller('AlertCtrl',AlertCtrl);

	AlertCtrl.$inject = ['alertService'];

	function AlertCtrl(alertService){
		var alert = this;
		alert.alertService = alertService;
	}
angular
	.module('occupassion')
	.service('alertService',alertService);

	alertService.$inject = [];
	function alertService(){
		var alertService = this;

		alertService.alertMessage = null;
		alertService.alertClass = null;
		alertService.showAlert = false;
	};